﻿using UnityEngine;
using System.Collections;

public class EnemyStun2 : MonoBehaviour {

	// if Player hits the stun point of the enemy, then call Stunned on the enemy
	void OnCollisionEnter2D(Collision2D other)
	{
		if (other.gameObject.tag == "Player")
		{
			// tell the enemy to be stunned
			this.GetComponentInParent<Enemy2>().Stunned();

			// Make the player bounce of the enemy
			other.gameObject.GetComponent<CharacterController2D>().EnemyBounce();
		}
	}
}
