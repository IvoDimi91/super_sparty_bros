﻿using UnityEngine;
using System.Collections;

namespace AssemblyCSharp
{
	public class Life : MonoBehaviour
	{

		public int lifeValue = 1;
		public bool taken = false;

		void OnTriggerEnter2D (Collider2D other)
		{
			if ((other.tag == "Player" ) && (!taken) && (other.gameObject.GetComponent<CharacterController2D>().playerCanMove))
			{
				// mark as taken so doesn't get taken multiple times
				taken=true;

				// if explosion prefab is provide, then instantiate it
//				if (explosion)
//				{
//					Instantiate(explosion,transform.position,transform.rotation);
//				}

				// do the player collect life thing
				other.gameObject.GetComponent<CharacterController2D>().AddLife(lifeValue);

				// destroy the life
				DestroyObject(this.gameObject);
			}
		}
	}
}

