﻿using UnityEngine;
using System.Collections;

namespace AssemblyCSharp
{
	public class EnemyDead2 : MonoBehaviour {

		// if Player hits the stun point of the enemy, then call Stunned on the enemy
		void OnCollisionEnter2D(Collision2D other)
		{
			if (other.gameObject.tag == "Projectile")
			{
				// tell the enemy to be stunned
				this.GetComponentInParent<Enemy2>().Dead();

				Destroy (other.gameObject);
				// Make the player bounce of the enemy
				//other.gameObject.GetComponent<CharacterController2D>().EnemyBounce();
			}
		}

		void OnTriggerEnter2D(Collider2D other)	{
			if (other.gameObject.tag == "Projectile")
			{
				// tell the enemy to be stunned
				this.GetComponentInParent<Enemy2>().Dead();

				Destroy (other.gameObject);


				// Make the player bounce of the enemy
				//other.gameObject.GetComponent<CharacterController2D>().EnemyBounce();
			}
		}
	}
}

